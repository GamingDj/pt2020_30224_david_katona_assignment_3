package model;

/**
 * Modelling of a product, having an id, name, quantity and a price
 * 
 * @author David
 *
 */
public class Product {

	private int productId;
	private String productName;
	private int productQuantity;
	private float productPrice;

	public Product() {

	}

	public Product(int productId, String productName, int productQuantity, float productPrice) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productQuantity = productQuantity;
		this.productPrice = productPrice;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}

	public float getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(float productPrice) {
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
		return productName + ", quantity: " + productQuantity + ", price:" + productPrice;
	}

}
