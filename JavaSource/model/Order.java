package model;

/**
 * Modelling of an order, having an id, name, product and the qunatity
 * 
 * @author David
 *
 */
public class Order {

	private int orderId;
	private String clientName;
	private String productName;
	private int quantity;

	public Order() {

	}

	public Order(int orderId, String clientName, String productName, int quantity) {
		super();
		this.orderId = orderId;
		this.clientName = clientName;
		this.productName = productName;
		this.quantity = quantity;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return clientName + ", product:" + productName + ", quantity: " + quantity;
	}

}
