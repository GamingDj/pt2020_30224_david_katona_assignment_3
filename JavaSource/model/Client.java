package model;

/**
 * Modelling of a client having an id, name and an address
 * 
 * @author David
 *
 */
public class Client {

	private int clientId;
	private String clientName;
	private String clientAddress;

	public Client() {
	}

	public Client(int clientId, String clientName, String clientAddress) {
		super();
		this.clientId = clientId;
		this.clientName = clientName;
		this.setClientAddress(clientAddress);
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	@Override
	public String toString() {
		return clientName + ", " + clientAddress;
	}

}
