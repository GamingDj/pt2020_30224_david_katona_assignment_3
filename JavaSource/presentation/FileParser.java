package presentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Class used to parse the text file into Command type.
 * 
 * @author David
 *
 */
public class FileParser {

	private File file;
	private Scanner scanner;

	/**
	 * Constructor that takes as an argument the path to the text file.
	 * 
	 * @param path File path
	 */
	public FileParser(String path) {

		file = new File(path);
		try {
			scanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Method used to identify and cast commands from text to Command.
	 * 
	 * @return The next Command in the file
	 */
	public Command nextCommand() {
		Command next = null;
		StringTokenizer comma;
		String line = scanner.nextLine();
		if (line.startsWith("Report") == true) {
			next = new Command("Report", line.substring(7));
		} else if (line.startsWith("Order") == true) {
			line = line.substring(7);
			comma = new StringTokenizer(line, ",");
			String name = comma.nextToken();
			String product = comma.nextToken().substring(1);
			int quantity = Integer.parseInt(comma.nextToken().substring(1));
			next = new Command("Order", name, product, quantity);
		} else if (line.startsWith("Insert") == true) {
			line = line.substring(7);
			if (line.startsWith("product") == true) {
				line = line.substring(9);
				comma = new StringTokenizer(line, ",");
				String product = comma.nextToken();
				int quantity = Integer.parseInt(comma.nextToken().substring(1));
				float price = Float.parseFloat(comma.nextToken().substring(1));
				next = new Command("Insert", "product", product, quantity, price);
			} else if (line.startsWith("client") == true) {
				line = line.substring(8);
				comma = new StringTokenizer(line, ",");
				String name = comma.nextToken();
				String address = comma.nextToken().substring(1);
				next = new Command("Insert", "client", name, address);
			}
		} else if (line.startsWith("Delete") == true) {
			line = line.substring(7);
			if (line.startsWith("product") == true) {
				line = line.substring(9);
				next = new Command("Delete", "product", line);
			} else if (line.startsWith("client") == true) {
				line = line.substring(8);
				comma = new StringTokenizer(line, ",");
				String name = comma.nextToken();
				String address = comma.nextToken().substring(1);
				next = new Command("Delete", "client", name, address);
			}
		}
		return next;
	}

	/**
	 * Returns true if there are more commands, false if it's at the end of file
	 * 
	 * @return True if there is at least one more line
	 */
	public boolean notEndOfFile() {
		return scanner.hasNextLine();
	}

	/**
	 * Method used to close the current file
	 */
	public void closeFile() {
		file = null;
		scanner.close();
	}

}
