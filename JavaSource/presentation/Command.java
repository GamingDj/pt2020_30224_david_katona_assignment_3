package presentation;

/**
 * Class used to store commands.
 * Multiple constructors exists for every type of possible command.
 * String variables are empty, and numerical types are initialized as 0
 * @author David
 *
 */
public class Command {

	private String type = "";
	private String target = "";
	private String client = "";
	private String address = "";
	private String product = "";
	private int quantity = 0;
	private float price = 0;

	// generate report
	public Command(String type, String target) {
		this.type = type;
		this.target = target;
	}

	// insert & delete client
	public Command(String type, String target, String client, String address) {
		this.type = type;
		this.target = target;
		this.client = client;
		this.address = address;
	}

	// insert product
	public Command(String type, String target, String product, int quantity, float price) {
		this.type = type;
		this.target = target;
		this.product = product;
		this.quantity = quantity;
		this.price = price;
	}

	// delete product
	public Command(String type, String target, String product) {
		this.type = type;
		this.target = target;
		this.product = product;
	}

	// order
	public Command(String type, String client, String product, int quantity) {
		this.type = type;
		this.client = client;
		this.product = product;
		this.quantity = quantity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		String s = "";
		s += type + " " + target + " " + client + " " + product + " " + address + " ";
		if (quantity != 0)
			s += quantity + " ";
		if (price != 0.0f)
			s += price + " ";
		return s;
	}

}
