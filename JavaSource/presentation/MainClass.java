package presentation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import businessLogicLayer.ClientBLL;
import businessLogicLayer.OrderBLL;
import businessLogicLayer.ProductBLL;
import dataAccessLayer.ProductDAO;

/**
 * Main class containing the public static void main(String[] args)
 * 
 * @author David
 *
 */
public class MainClass {

	protected static final Logger LOGGER = Logger.getLogger(MainClass.class.getName());
	static ClientBLL cl = new ClientBLL();
	static ProductBLL pr = new ProductBLL();
	static OrderBLL or = new OrderBLL();

	public static void main(String[] args) throws SQLException {

		FileParser parser = new FileParser(args[0]);
		while (parser.notEndOfFile() == true) {
			Command c = parser.nextCommand();
			if (c.getType().compareTo("Report") == 0) {
				if (c.getTarget().compareTo("client") == 0) {
					cl.showAll();
				} else if (c.getTarget().compareTo("product") == 0) {
					pr.showAll();
				} else if (c.getTarget().compareTo("order") == 0) {
					or.showAll();
				}
			} else if (c.getType().compareTo("Insert") == 0) {
				if (c.getTarget().compareTo("client") == 0) {
					cl.insertClient(c.getClient(), c.getAddress());
				} else if (c.getTarget().compareTo("product") == 0) {
					pr.insertProduct(c.getProduct(), c.getQuantity(), c.getPrice());
				}
			} else if (c.getType().compareTo("Delete") == 0) {
				if (c.getTarget().compareTo("client") == 0) {
					cl.deleteClient(c.getClient(), c.getAddress());
				} else if (c.getTarget().compareTo("product") == 0) {
					pr.deleteProduct(c.getProduct());
				}
			} else if (c.getType().compareTo("Order") == 0) {
				if (or.insertOrder(c.getClient(), c.getProduct(), c.getQuantity()) == 0) {
					OrderBLL.setOrderNr(OrderBLL.getOrderNr() + 1);
					String file_name = "RaportComanda-" + OrderBLL.getOrderNr() + ".pdf";
					Document doc = new Document();
					try {
						PdfWriter.getInstance(doc, new FileOutputStream(file_name));
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (DocumentException e) {
						e.printStackTrace();
					}
					doc.open();
					Paragraph para = new Paragraph("Raport Comanda");
					try {
						doc.add(para);
						doc.add(Chunk.NEWLINE);
						para = new Paragraph("Stoc insuficient!");
						para.setAlignment(Element.ALIGN_CENTER);
						doc.add(para);

					} catch (DocumentException e1) {
						e1.printStackTrace();
					}

					doc.close();
				} else {
					OrderBLL.setOrderNr(OrderBLL.getOrderNr() + 1);
					String file_name = "RaportComanda-" + OrderBLL.getOrderNr() + ".pdf";
					Document doc = new Document();
					try {
						PdfWriter.getInstance(doc, new FileOutputStream(file_name));
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (DocumentException e) {
						e.printStackTrace();
					}
					doc.open();
					Paragraph para = new Paragraph("Raport Comanda");
					try {
						doc.add(para);
						doc.add(Chunk.NEWLINE);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
					PdfPTable table = new PdfPTable(4);
					Font bold = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
					PdfPCell cell = new PdfPCell(new Phrase("Nume", bold));
					table.addCell(cell);
					cell = new PdfPCell(new Phrase("Articol", bold));
					table.addCell(cell);
					cell = new PdfPCell(new Phrase("Cantitate", bold));
					table.addCell(cell);
					cell = new PdfPCell(new Phrase("Total", bold));
					table.addCell(cell);
					table.setHeaderRows(1);
					ProductDAO productDAO = new ProductDAO();
					table.addCell(c.getClient());
					table.addCell(c.getProduct());
					table.addCell(Integer.toString(c.getQuantity()));
					table.addCell(Float.toString(
							c.getQuantity() * productDAO.findByProductName(c.getProduct()).getProductPrice()));
					try {
						doc.add(table);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
					doc.close();
				}

			}
		}

	}
}
