package presentation;

import java.lang.reflect.Field;

/**
 * Class used for Reflection properties
 * 
 * @author David
 *
 */
public class Reflection {

	public Reflection() {
		super();

	}

	/**
	 * Method used to retriever all values from all fields of a variable.
	 * 
	 * @param o      The variable
	 * @param needId if false: returns all values except the id
	 * @return A String containing all values, separated by commas
	 */
	public static String retrieveValues(Object o, boolean needId) {

		String s = "";
		for (Field field : o.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value;
			try {
				value = field.get(o);
				if (field.getType().getName().compareTo("java.lang.String") == 0)
					s += "'" + value + "'" + ",";
				else
					s += value + ",";
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		s = s.substring(0, s.length() - 1); // cut last comma
		if (needId == false) {
			s = s.substring(s.indexOf(",") + 1); // cut id
		}
		return s;
	}

	/**
	 * Method used to retrieve the name of all fields of a variable
	 * 
	 * @param o The variable
	 * @return A String containing all the fields of a variable, except the id
	 */
	public static String retrieveFields(Object o) {

		String s = "";
		for (Field field : o.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				s += field.getName() + ",";
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		s = s.substring(0, s.length() - 1); // cut last comma
		s = s.substring(s.indexOf(",") + 1); // cut id
		return s;
	}

	/**
	 * Method used to retrieve the name of a variable
	 * 
	 * @param o The variable
	 * @return A String with the name
	 */
	public static String retrieveName(Object o) {
		String s = "";
		for (Field field : o.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value;
			try {
				if (field.getName().contains("Name") == true) {
					value = field.get(o);
					s = "'" + (String) value + "'";
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return s;
	}

	/**
	 * Method used to retrieve the address of a client
	 * 
	 * @param o A client
	 * @return A String with the address
	 */
	public static String retrieveClientAddress(Object o) {
		String s = "";
		for (Field field : o.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value;
			try {
				if (field.getName().compareTo("clientAddress") == 0) {
					value = field.get(o);
					s = "'" + (String) value + "'";
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return s;
	}

	/**
	 * Method used to retrieve the quantity of a variable
	 * 
	 * @param o The variable
	 * @return An int containing the quantity
	 */
	public static int retrieveQuantity(Object o) {
		for (Field field : o.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value;
			try {
				if (field.getName().contains("uantity") == true) {
					value = field.get(o);
					return (int) value;
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
}
