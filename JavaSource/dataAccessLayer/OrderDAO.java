package dataAccessLayer;

import model.Order;

/**
 * Order Database Access
 * 
 * @author David
 *
 */
public class OrderDAO extends AbstractDAO<Order> {

	public OrderDAO() {
		super();
	}

}
