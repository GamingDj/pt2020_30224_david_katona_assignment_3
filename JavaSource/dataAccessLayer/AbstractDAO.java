package dataAccessLayer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import connection.ConnectionFactory;
import presentation.Reflection;

/**
 * A generic class containing common operations and access to the database
 * 
 * @author David
 *
 * @param <T> The Type (Client, Order or Product)
 */
public class AbstractDAO<T> {
	protected final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
	Reflection reflection;
	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	/**
	 * Creates a SELECT-FROM-WHERE query
	 * 
	 * @param field Name of the column
	 * @return A String containing a MySQL query: SELECT * FROM Table WHERE field =
	 *         ?
	 */
	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}

	/**
	 * Creates a SELECT-FROM query
	 * 
	 * @return A String containing a MySQL query: SELECT * FROM Table
	 */
	private String createSelectAllQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM ");

		if (type.getSimpleName().toLowerCase().compareTo("order") == 0)
			sb.append("`" + type.getSimpleName().toLowerCase() + "`");
		else
			sb.append(type.getSimpleName().toLowerCase());

		return sb.toString();
	}

	/**
	 * Create a MySQL INSERT INTO query
	 * 
	 * @param values A String containing the values to be inserted, separated by
	 *               commas
	 * @return A String containing a MySQL query: INSERT INTO Table (field1, field2,
	 *         ...) VALUES (val1, val2, ...)
	 */
	private String createInsertQuery(String values) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		if (type.getSimpleName().toLowerCase().compareTo("order") == 0)
			sb.append("`" + type.getSimpleName().toLowerCase() + "`");
		else
			sb.append(type.getSimpleName().toLowerCase());
		try {
			sb.append(" (" + Reflection.retrieveFields(type.newInstance()) + ")");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		sb.append(" VALUES (" + values + ")");
		return sb.toString();
	}

	/**
	 * Searches for a product
	 * 
	 * @param name Name of the product
	 * @return A Product named name, null if the product was not found
	 */
	public T findByProductName(String name) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery(type.getSimpleName().toLowerCase() + "Name");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1, name);
			resultSet = statement.executeQuery();
			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findByProductName " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * Creates A List of concrete objects based on the given result set
	 * 
	 * @param resultSet The result set
	 * @return A List with the T type objects
	 */
	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Generic method used for showing all elements of a table
	 * 
	 * @return A List with all the elements of a table
	 */
	public List<T> selectAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectAllQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:selectAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * Generic method used for inserting into the database
	 * 
	 * @param t The object to be inserted
	 */
	public void insert(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createInsertQuery(Reflection.retrieveValues(t, false));
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}

	/**
	 * Generic method used for deleting from the database
	 * 
	 * @param t The object to delete
	 */
	public void delete(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = "";
		if (t.getClass().getSimpleName().compareTo("Product") == 0) {
			query = "DELETE FROM " + type.getSimpleName().toLowerCase() + " WHERE productName="
					+ Reflection.retrieveName(t);
		} else if (t.getClass().getSimpleName().compareTo("Client") == 0) {
			query = "DELETE FROM " + type.getSimpleName().toLowerCase() + " WHERE clientName="
					+ Reflection.retrieveName(t) + " AND clientAddress=" + Reflection.retrieveClientAddress(t);
		}
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}

	/**
	 * Method used for removing duplicates from the Products table. It uses multiple
	 * MySQL statements.
	 */
	public void removeDuplicates() {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = "";
		query = "USE schooldb;";
		connection = ConnectionFactory.getConnection();
		try {
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
			query = "DROP TABLE IF EXISTS temp;";
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
			query = "CREATE TABLE `schooldb`.`temp` (\r\n" + "    `productId` INT NOT NULL AUTO_INCREMENT,\r\n"
					+ "    `productName` VARCHAR(45) NULL DEFAULT NULL,\r\n"
					+ "    `productQuantity` INT NULL DEFAULT NULL,\r\n" + "    `productPrice` FLOAT NULL,\r\n"
					+ "    PRIMARY KEY (`productId`)\r\n" + ");";
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
			query = "INSERT INTO temp (`productId`, `productName`, `productQuantity`, `productPrice`)\r\n"
					+ "SELECT productId, productName, SUM(productQuantity), productPrice\r\n"
					+ "FROM product       \r\n" + "GROUP BY productName;";
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
			query = "DROP TABLE `schooldb`.`product`;";
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
			query = "RENAME TABLE temp TO `schooldb`.`product`;";
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method used to check if there are enough products in the databse, and to
	 * update the current amount of products
	 * 
	 * @param product  Name of the product
	 * @param quantity The quantity to be sold
	 * @param currQ    The current quantity (current stock)
	 * @return 0 if there were not enough products, 1 if the transaction was
	 *         succcessful
	 */
	public int updateProducts(String product, int quantity, int currQ) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = "";
		if (currQ >= quantity)
			query = "UPDATE product SET productQuantity=(productQuantity-" + quantity + ") WHERE productName=\""
					+ product + "\"";
		else
			return 0;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return 1;
	}
}