package dataAccessLayer;

import model.Product;

/**
 * Product Database Access
 * 
 * @author David
 *
 */
public class ProductDAO extends AbstractDAO<Product> {

	public ProductDAO() {
		super();
	}

}
