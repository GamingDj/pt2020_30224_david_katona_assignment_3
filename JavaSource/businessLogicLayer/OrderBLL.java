package businessLogicLayer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import dataAccessLayer.OrderDAO;
import dataAccessLayer.ProductDAO;
import model.Order;

/**
 * Operations with orders
 * 
 * @author David
 *
 */
public class OrderBLL {

	private OrderDAO orderDAO;
	/**
	 * Static variable used to store the number of orders
	 */
	private static int orderNr;

	public OrderBLL() {
		orderNr = 0;
		this.orderDAO = new OrderDAO();
	}

	/**
	 * Creates a new order in the database
	 * 
	 * @param clientName  Name of the client
	 * @param productName Name of the product(s)
	 * @param quantity    Quantity of products
	 * @return 1 if the insertion was successful, 0 otherwise
	 */
	public int insertOrder(String clientName, String productName, int quantity) {

		ProductDAO productDAO = new ProductDAO();
		int currQ = productDAO.findByProductName(productName).getProductQuantity();
		if (orderDAO.updateProducts(productName, quantity, currQ) == 1) {
			orderDAO.insert(new Order(0, clientName, productName, quantity));
			return 1;
		}
		return 0;
	}

	public static int getOrderNr() {
		return orderNr;
	}

	public static void setOrderNr(int orderNr) {
		OrderBLL.orderNr = orderNr;
	}

	/**
	 * Displays all orders in the database and stores them in a PDF file
	 */
	public void showAll() {
		String file_name = "RaportComenzi.pdf";
		Document doc = new Document();
		try {
			PdfWriter.getInstance(doc, new FileOutputStream(file_name));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		doc.open();
		Paragraph para = new Paragraph("Raport Comanda");
		try {
			doc.add(para);
			doc.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		PdfPTable table = new PdfPTable(4);
		Font bold = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
		PdfPCell cell = new PdfPCell(new Phrase("Nume", bold));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Articol", bold));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Cantitate", bold));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Total", bold));
		table.addCell(cell);
		table.setHeaderRows(1);
		ProductDAO productDAO = new ProductDAO();
		for (Order o : orderDAO.selectAll()) {

			table.addCell(o.getClientName());
			table.addCell(o.getProductName());
			table.addCell(Integer.toString(o.getQuantity()));
			table.addCell(Float
					.toString(o.getQuantity() * productDAO.findByProductName(o.getProductName()).getProductPrice()));
		}
		try {
			doc.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		doc.close();

	}

}
