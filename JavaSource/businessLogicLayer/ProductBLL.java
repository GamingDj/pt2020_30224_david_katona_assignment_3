package businessLogicLayer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import dataAccessLayer.ProductDAO;
import model.Product;

/**
 * Operations with products
 * 
 * @author David
 *
 */
public class ProductBLL {

	private ProductDAO productDAO;

	public ProductBLL() {
		productDAO = new ProductDAO();
	}

	/**
	 * Inserts a product into the database
	 * 
	 * @param name     Name of product
	 * @param quantity Quantity to be inserted
	 * @param price    Price of 1 unit
	 */
	public void insertProduct(String name, int quantity, float price) {
		productDAO.insert(new Product(0, name, quantity, price));
		productDAO.removeDuplicates();
	}

	/**
	 * Deletes a product from the database
	 * 
	 * @param name Name of the product
	 */
	public void deleteProduct(String name) {
		productDAO.delete(new Product(0, name, 0, 0));
	}

	/**
	 * Displays all products in the database and stores them in a PDF file
	 */
	public void showAll() {
		String file_name = "RaportProduse.pdf";
		Document doc = new Document();
		try {
			PdfWriter.getInstance(doc, new FileOutputStream(file_name));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		doc.open();
		Paragraph para = new Paragraph("Raport Produse");
		try {
			doc.add(para);
			doc.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PdfPTable table = new PdfPTable(3);
		Font bold = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
		PdfPCell cell = new PdfPCell(new Phrase("Articol", bold));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Cantitate", bold));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Pret", bold));
		table.addCell(cell);
		table.setHeaderRows(1);
		for (Product p : productDAO.selectAll()) {
			table.addCell(p.getProductName());
			table.addCell(Integer.toString(p.getProductQuantity()));
			table.addCell(Float.toString(p.getProductPrice()));
		}
		try {
			doc.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		doc.close();
	}

}
