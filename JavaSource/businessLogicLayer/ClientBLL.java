package businessLogicLayer;

import dataAccessLayer.ClientDAO;
import model.Client;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Operations with clients
 * 
 * @author David
 *
 */
public class ClientBLL {

	private ClientDAO clientDAO;

	public ClientBLL() {

		clientDAO = new ClientDAO();
	}

	/**
	 * Inserts a client into the database
	 * 
	 * @param name    Name of the client
	 * @param address Address of the client
	 */
	public void insertClient(String name, String address) {
		clientDAO.insert(new Client(0, name, address));
	}

	/**
	 * Deletes a client from the database
	 * 
	 * @param name    Name of the client
	 * @param address Address of the client
	 */
	public void deleteClient(String name, String address) {
		clientDAO.delete(new Client(0, name, address));
	}

	/**
	 * Display all clients in the database and stores them in a PDF file
	 */
	public void showAll() {

		String file_name = "RaportClienti.pdf";
		Document doc = new Document();
		try {
			PdfWriter.getInstance(doc, new FileOutputStream(file_name));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		doc.open();
		Paragraph para = new Paragraph("Raport Clienti");
		try {
			doc.add(para);
			doc.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		PdfPTable table = new PdfPTable(2);
		Font bold = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
		PdfPCell cell = new PdfPCell(new Phrase("Nume", bold));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Adresa", bold));
		table.addCell(cell);
		table.setHeaderRows(1);
		for (Client c : clientDAO.selectAll()) {
			table.addCell(c.getClientName());
			table.addCell(c.getClientAddress());
		}
		try {
			doc.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		doc.close();
	}
}
