DROP DATABASE IF EXISTS schooldb;
CREATE DATABASE schooldb;
USE schooldb;

DROP USER IF EXISTS 'david'@'localhost';
CREATE USER 'david'	@'localhost' IDENTIFIED BY 'tp2020';
GRANT ALL PRIVILEGES ON	schooldb.* TO 'david'@'localhost';

DROP TABLE IF EXISTS  `schooldb`.`client`;
CREATE TABLE `schooldb`.`client` (
    `clientId` INT NOT NULL AUTO_INCREMENT,
    `clientName` VARCHAR(45) NULL,
	`clientAddress` VARCHAR(45) NULL,
    PRIMARY KEY (`clientId`)
);
  
DROP TABLE IF EXISTS `schooldb`.`product`;
CREATE TABLE `schooldb`.`product` (
    `productId` INT NOT NULL AUTO_INCREMENT,
    `productName` VARCHAR(45) NULL DEFAULT NULL,
    `productQuantity` INT NULL DEFAULT NULL,
    `productPrice` FLOAT NULL,
    PRIMARY KEY (`productId`)
);
  
DROP TABLE IF EXISTS `schooldb`.`order`;
CREATE TABLE `schooldb`.`order` (
    `orderId` INT NOT NULL AUTO_INCREMENT,
    `clientName` VARCHAR(45) NULL,
    `productName` VARCHAR(45) NULL DEFAULT NULL,
    `quantity` INT NULL,
    PRIMARY KEY (`orderId`)
);

DROP TABLE IF EXISTS `schooldb`.`employees`;
CREATE TABLE `schooldb`.`employees` (
  `employeeId` INT NOT NULL AUTO_INCREMENT,
  `employeeName` VARCHAR(45) NULL,
  `employeeRole` VARCHAR(45) NULL,
  `employeeSalary` INT NULL,
  PRIMARY KEY (`employeeId`));
  
  DROP TABLE IF EXISTS `schooldb`.`shops`;
  CREATE TABLE `schooldb`.`shops` (
  `shopId` INT NOT NULL AUTO_INCREMENT,
  `shopLocation` VARCHAR(45) NULL,
  `shopEmployees` INT NULL,
  PRIMARY KEY (`shopId`));



  
