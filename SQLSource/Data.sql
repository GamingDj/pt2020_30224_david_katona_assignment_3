USE schooldb;

INSERT INTO `schooldb`.`client` (`clientName`, `clientAddress`) VALUES ("Ioan Ispas", "Zalau");
INSERT INTO `schooldb`.`client` (`clientName`, `clientAddress`) VALUES ("Marcel Nagy", "Brasov");
INSERT INTO `schooldb`.`client` (`clientName`, `clientAddress`) VALUES ("Roxana Pop", "Constanta");
INSERT INTO `schooldb`.`client` (`clientName`, `clientAddress`) VALUES ("Andrei Muresan", "Cluj-Napoca");
INSERT INTO `schooldb`.`client` (`clientName`, `clientAddress`) VALUES ("Vasile Pop", "Bucuresti");

INSERT INTO `schooldb`.`product` (`productName`, `productQuantity`,  `productPrice`) VALUES ("apple", 20, 1);
INSERT INTO `schooldb`.`product` (`productName`, `productQuantity`,  `productPrice`) VALUES ("lemon", 29, 2);
INSERT INTO `schooldb`.`product` (`productName`, `productQuantity`,  `productPrice`) VALUES ("melon", 40, 2.5);
INSERT INTO `schooldb`.`product` (`productName`, `productQuantity`,  `productPrice`) VALUES ("banana", 86, 1.2);
INSERT INTO `schooldb`.`product` (`productName`, `productQuantity`,  `productPrice`) VALUES ("pear", 27, 2.5);

INSERT INTO `schooldb`.`order` (`clientName`, `productName`, `quantity`) VALUES ("Roxana Pop", "apple", 7);
INSERT INTO `schooldb`.`order` (`clientName`, `productName`, `quantity`) VALUES ("Matei Mare", "pear", 10);
INSERT INTO `schooldb`.`order` (`clientName`, `productName`, `quantity`) VALUES ("Andrei Muresan", "banana", 15);
INSERT INTO `schooldb`.`order` (`clientName`, `productName`, `quantity`) VALUES ("Vasile Pop", "apple", 5);
INSERT INTO `schooldb`.`order` (`clientName`, `productName`, `quantity`) VALUES ("Ioan Ispas", "lemon", 30);

INSERT INTO `schooldb`.`employees` (`employeeName`,  `employeeRole`, `employeeSalary`) VALUES ("Michael Scofield", "CEO", 5000);
INSERT INTO `schooldb`.`employees` (`employeeName`,  `employeeRole`, `employeeSalary`) VALUES ("Tony Montana", "Salesman", 2000);
INSERT INTO `schooldb`.`employees` (`employeeName`,  `employeeRole`, `employeeSalary`) VALUES ("Lincoln Burrows", "Janitor", 1500);

INSERT INTO `schooldb`.`shops` (`shopLocation`, `shopEmployees`) VALUES ("Cluj-Napoca", 8);
INSERT INTO `schooldb`.`shops` (`shopLocation`, `shopEmployees`) VALUES ("Bucuresti", 7);
INSERT INTO `schooldb`.`shops` (`shopLocation`, `shopEmployees`) VALUES ("Timisoara", 8);
INSERT INTO `schooldb`.`shops` (`shopLocation`, `shopEmployees`) VALUES ("Zalau", 10);